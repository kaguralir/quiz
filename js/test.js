const start_btn = document.querySelector(".start_btn button");
const start_btn2 = document.querySelector(".start_btn2 button");
const start_btn3 = document.querySelector(".start_btn3 button");

const categoryType = document.querySelector(".category");
const score = document.querySelector(".score");

const info_box = document.querySelector(".info_box");
const exit_btn = info_box.querySelector(".buttons .quit");
const continue_btn = info_box.querySelector(".buttons .restart");
const quiz_box = document.querySelector(".quiz_box");
const result_box = document.querySelector(".result_box");
const option_list = document.querySelector(".option_list");
const time_line = document.querySelector("header .time_line");
const timeText = document.querySelector(".timer .time_left_txt");
const timeCount = document.querySelector(".timer .timer_sec");

const mediaQuery = window.matchMedia("(max-width: 650px)");

let timeValue = 30;
let i = 0;
let que_numb = 1;
let userPoints = 0;
let counter;
let counterLine;
let widthValue = 0;

const restart_quiz = result_box.querySelector(".buttons .restart");
const quit_quiz = result_box.querySelector(".buttons .quit");

const questionsOption0 = ["10/2?", "30*3?", "12^2?", "4 au carré?", "-12 - 5?"];

const avalibleAnswersOption0 = [
  ["5", "2", "3", "10"],
  ["14", "82", "90", "20"],
  ["24", "2", "6", "144"],
  ["4", "7", "2", "16"],
  ["17", "1", "0", "12"],
];

const correctAnswersOption0 = ["0", "2", "3", "2", "1"]; //bonne réponses "categorie 1"

const questionsOption0Difficulty1 = [
  "42/6?",
  "7 x 65?",
  "carré de 49?",
  "Pi?",
  "x -> 46 = 5x + 6?",
];

const avalibleAnswersOption0Difficulty1 = [
  ["7", "8", "36", "48"],
  ["$72", "$425", "$455", "$485"],
  ["16", "45", "12", "7"],
  ["3.14", "2.75", "1.41", "5.19"],
  ["4", "7", "8", "9"],
];

const correctAnswersOption0Difficulty1 = ["1", "2", "3", "0", "2"]; //bonne réponse catégorie

const questionsOption0Difficulty2 = [
  "Suite de  4, 5, 8, 10, 12, 9, 6, 5, 11",
  " 6 x 379?",
  "1 cm en feet?",
  "5400/90?",
  "L'aire d'un rectangle dont chaque côté a doublé?",
];

const avalibleAnswersOption0Difficulty2 = [
  ["12", "10", "8", "6"],
  ["180", "240", "1800", "2400"],
  ["1", "0.03", "0.07", "0.5"],
  ["6", "60", "600", "6000"],
  [
    "L'aire est multipliée par 2",
    "L'aire est multipliée par  4",
    "L'aire est multipliée par  6",
    "L'aire est multipliée par  8",
  ],
];

const correctAnswersOption0Difficulty2 = [2, 3, 1, 1, 1]; //bonne réponse catégorie 3

let categories;
let questions;
let avalibleAnswers;
let correctAnswers;

let tickIconTag = '<div class="icon tick"></div>';
let crossIconTag = '<div class="icon cross"></div>';
// let nextIcontag =
//   '<div class="next_font"><i class="far fa-arrow-alt-circle-right"></i></div>';

start_btn.onclick = () => {
  quiz_box.classList.add("activeQuiz");

  categories = "Categorie 1";

  questions = questionsOption0;
  avalibleAnswers = avalibleAnswersOption0;
  correctAnswers = correctAnswersOption0;
  console.log("categorie 1");
  main();
};

start_btn2.onclick = () => {
  quiz_box.classList.add("activeQuiz"); //show quiz box

  categories = "Categorie 2";

  questions = questionsOption0Difficulty1;
  avalibleAnswers = avalibleAnswersOption0Difficulty1;
  correctAnswers = correctAnswersOption0Difficulty1;
  console.log("categorie 2");
  main();
};

start_btn3.onclick = () => {
  quiz_box.classList.add("activeQuiz"); //show quiz box

  categories = "Categorie 3";

  questions = questionsOption0Difficulty2;
  avalibleAnswers = avalibleAnswersOption0Difficulty2;
  correctAnswers = correctAnswersOption0Difficulty2;
  console.log("categorie 3");
  main();
};

quit_quiz.onclick = () => {
  window.location.reload(); //reload the current window
};

const next_btn = document.querySelector("footer .next_btn");
const bottom_ques_counter = document.querySelector("footer .total_que");

function main() {
  queCounter(1); //nb de question
  startTimer(30); //nb de secondes
  startTimerLine(0); //depart de ligne

  next_btn.onclick = () => {
    /*     if (mediaQuery.matches) {
      // Then trigger an alert

      next_btn.insertAdjacentHTML("beforeend", nextIcontag);
      next_btn.style.backgroundColor = "transparent";
    } */
    console.log(i, questions[i]);
    clearInterval(counter); //remettre a zero si question suivante choisie
    clearInterval(counterLine); //remettre a zero si question suivante choisie
    if (i < questions.length - 1) {
      i++;
  const rightAnswer = correctAnswers[i];
      main(i);
      next_btn.classList.remove("show");
      que_numb++;
      queCounter(que_numb);
    } else {
      console.log("pas de questions restantes");
      showResult();
    }
  };

  function startTimer(time) {
    counter = setInterval(timer, 1000);
    function timer() {
      timeCount.textContent = time; //temps délimité
      time--;
      if (time < 9) {
        //rajoute un "0" si le temps est inférieur à 10
        let addZero = timeCount.textContent;
        timeCount.textContent = "0" + addZero;
      }
      if (time < 0) {
        clearInterval(counter); //compteur stoppe à 0
        timeText.textContent = "";
        const allOptions = option_list.children.length;
        let correcAns = rightAnswer; //bonne réponse automatiquement choisie à la fin du compteur
        console.log("correcAns timer is " + correcAns);

        for (m = 0; m < allOptions; m++) {
          const realAnswer = option_list.children[m];

          if ([m] == rightAnswer) {
            realAnswer.classList.add("correct");
            realAnswer.insertAdjacentHTML("beforeend", tickIconTag);
            realAnswer.setAttribute("class", "option correct");
            console.log("Bonne réponse auto-sélectionné");
          }
        }
        for (m = 0; m < allOptions; m++) {
          option_list.children[m].classList.add("disabled");
        }
        next_btn.classList.add("show"); //bouton suivant apparait si une réponse est choisie
      }
    }
  }

  const que_text = document.querySelector(".que_text");

  categoryType.textContent = categories;
  next_btn.classList.remove("show");

  console.log("\x1b[44m%s\x1b[0m", "Welcome to Quiz Game!");

  console.log(i, questions[i]);

  let currentQuestion = questions[i];
  let currentAvailableAnswers = avalibleAnswers[i];

  console.log("\x1b[45m%s\x1b[0m", "tu as " + userPoints + " points!");

  console.log("\x1b[33m%s\x1b[0m", "Question");
  console.log("\x1b[44m%s\x1b[0m", currentQuestion);
  console.log("\x1b[33m%s\x1b[0m", "Avalible Answers");

  let que_tag = "<span>" + currentQuestion + "</span>";
  let option_tag =
    '<div class="option"><span>' +
    currentAvailableAnswers[0] +
    "</span></div>" +
    '<div class="option"><span>' +
    currentAvailableAnswers[1] +
    "</span></div>" +
    '<div class="option"><span>' +
    currentAvailableAnswers[2] +
    "</span></div>" +
    '<div class="option"><span>' +
    currentAvailableAnswers[3] +
    "</span></div>";

  que_text.innerHTML = que_tag; //nouveau span dans le que_tag
  option_list.innerHTML = option_tag; //novuelle(s) div(s) par options qui sera utilisée plus tard pour lui attribuée des couleurs et des classes

  const option = option_list.querySelectorAll(".option");

  const rightAnswer = correctAnswers[i];
  const allOptions = option_list.children.length;

  // basée sur l'exemple : for (let l = 0, len = option.length; l < len; l++) {
  for (let k = 0, len = option.length; k < len; k++) {
    //liste des options
    (function (index) {
      option[k].onclick = function () {
        // alert(index);
        console.log([k]);
        const userAnswer = [k]; //option specifiquement choisie par l'utilisateur
        next_btn.classList.add("show");

        console.log(
          "choisir entre nb de array : " + i + " et index : " + rightAnswer
        );

        for (m = 0; m < allOptions; m++) {
          option_list.children[m].classList.add("disabled"); //empeche de choisir une autre réponse si une est deja choisie
        }

        if (userAnswer == rightAnswer) {
          option[k].classList.add("correct"); //classe pour les réponses justes rajouter à la div créer plus haut
          option[k].insertAdjacentHTML("beforeend", tickIconTag); //icone pour le reponses justes
          userPoints = userPoints + 10;
          console.log("\x1b[42m%s\x1b[0m", "juste");
          console.log("Bonne réponse");
          console.log("Score = " + userPoints);
          score.textContent = "Score : " + userPoints;
          // 10 points et non 1 par bonne réponse
        } else {
          option[k].classList.add("incorrect"); //classe pour les réponses fausses
          option[k].insertAdjacentHTML("beforeend", crossIconTag); //icone pour les réponses fausses
          console.log("\x1b[41m%s\x1b[0m", "faux", rightAnswer);
          console.log(" array choisi " + userAnswer);
          console.log(" bonne réponse est : " + rightAnswer);
          console.log("Score = " + userPoints);
          score.textContent = "Score : " + userPoints;

          for (m = 0; m < allOptions; m++) {
            const realAnswer = option_list.children[m];

            if ([m] == rightAnswer) {
              //si une réponse fausse a été choisie, la réponse vraie apparaît quand même
              realAnswer.classList.add("correct");
              realAnswer.insertAdjacentHTML("beforeend", tickIconTag);
              realAnswer.setAttribute("class", "option correct");
              console.log("Bonne réponse auto-sélectionné");
            }
          }
        }
      };
    })(k);
  }
}

function showResult() {
  info_box.classList.remove("activeInfo"); //quiz info enlevé à la fin
  quiz_box.classList.remove("activeQuiz"); //quiz box enlevé à la fin
  result_box.classList.add("activeResult"); //box résultat montré
  let questionsLength = questions.length;

  const scoreText = result_box.querySelector(".score_text");
  if (userPoints > 3) {
    let scoreTag =
      "<span>Bravo! 🎉, tu as <p>" + userPoints + "</p>  points.  <p>";
    /*       questionsLength +
      "</p></span>"; */
    scoreText.innerHTML = scoreTag;
  } else if (userPoints > 1) {
    let scoreTag =
      "<span>pas mal 😎, tu as <p>" + userPoints + "</p> points. <p>";
    /*       questionsLength +
      "</p></span>"; */
    scoreText.innerHTML = scoreTag;
  } else {
    let scoreTag =
      "<span>oh 😐, tu as seulement <p>" +
      userPoints +
      "</p>  points.  <p>"; /* +
      questionsLength +
      "</p></span>"; */
    scoreText.innerHTML = scoreTag;
  }
}

function startTimerLine(time) {
  if (mediaQuery.matches) {
    //version responsive de la ligne chronométrée
    counterLine = setInterval(timer, 85);
    function timer() {
      time += 1; 
      time_line.style.width = time + "px";
      if (time > 320) {
        clearInterval(counterLine); 
      }
    }
  } else {
    counterLine = setInterval(timer, 45);
    function timer() {
      time += 1; //temps
      time_line.style.width = time + "px"; //ligne de temps
      if (time > 549) {
        //taille de ligne
        clearInterval(counterLine); //cstoppé après une certaine valeur
      }
    }
  }
}
function queCounter(index) {
  if (mediaQuery.matches) {
    //version responsive du "nb de questions restantes sur x"
        // alert("Version responsive");
    let totalQueCounTag = "<span><p>" + index + "</p> / <p>" + questions.length;
    bottom_ques_counter.innerHTML = totalQueCounTag;
  } else {
    let totalQueCounTag =
      "<span><p>" +
      index +  
      "</p> sur <p>" +
      questions.length +
      "</p> Questions</span>";
    bottom_ques_counter.innerHTML = totalQueCounTag; //affichage du nb en html
  }
}

// main();
